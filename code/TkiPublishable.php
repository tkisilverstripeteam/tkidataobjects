<?php

/**
 * Makes some publishing related features from TkiPublishable available for other DataObjects.
 */
class TkiPublishable extends DataExtension {
	/* ---- Static variables ---- */
	
	/* ---- Instance variables ---- */
	protected $_cache_statuses = null;
    
	/* ---- Static methods ---- */
	
	/* ---- Instance methods ---- */
	
    public function getStatusField($cached = true)
    {
        $statuses = $this->getStatuses();
        $out = array();
        foreach($statuses as $status) {
            $out[] = $status['title'];
        }
        return implode(', ', $out);
    }
    /**
     * Provides information related to publish status.
	 *
	 * @param bool $cached Whether to serve the fields from cache; false regenerate them
	 * @return array
	 */
	public function getStatuses($cached = true) {
		if(!$this->_cache_statuses || !$cached) {
			$statuses = array();
			if($this->getIsDeletedFromStage()) {
                // Removed from draft
				if($this->getExistsOnLive()) {
					$statuses['removedfromdraft'] = array(
						'title' => _t('TkiPublishable.RemovedFromDraftTitle', 'Removed from draft'),
						'desc' => _t('TkiPublishable.RemovedFromDraftDesc', 'Item is published, but has been removed from draft'),
					);
				} 
                // Archived
                else {
					$statuses['archived'] = array(
						'title' => _t('TkiPublishable.ArchivedTitle', 'Archived'),
						'desc' => _t('TkiPublishable.ArchivedDesc', 'Item has been removed from draft and live'),
					);
				}
			} 
            // Draft
            else if($this->getIsAddedToStage()) {
				$statuses['addedtodraft'] = array(
					'title' => _t('TkiPublishable.DraftTitle', 'Draft'),
					'desc' => _t('TkiPublishable.DraftDesc', "Item has not been published yet")
				);
			} 
            // Modified
            else if($this->getIsModifiedOnStage()) {
				$statuses['modified'] = array(
					'title' => _t('TkiPublishable.ModifiedTitle', 'Modified'),
					'desc' => _t('TkiPublishable.ModifiedDesc', 'Item has unpublished changes'),
				);
			}
            // Published
            else if($this->getExistsOnLive()) {
				$statuses['published'] = array(
					'title' => _t('TkiPublishable.PublishedTitle', 'Published'),
					'desc' => _t('TkiPublishable.PublishedDesc', 'Item has been published'),
				);
			}
            
			$this->_cache_statuses = $statuses;
		}

		return $this->_cache_statuses;
	}
    
    /**
	 * Compares current draft with live version, and returns true if no draft version of this page exists  but the page
	 * is still published (eg, after triggering "Delete from draft site" in the CMS).
	 *
	 * @return bool
	 */
	public function getIsDeletedFromStage() {
		if(!$this->owner->ID) return true;
		if($this->isNew()) return false;

		$stageVersion = Versioned::get_versionnumber_by_stage(get_class($this->owner), 'Stage', $this->owner->ID);

		// Return true for both completely deleted pages and for pages just deleted from stage
		return !($stageVersion);
	}

	/**
	 * Return true if this page exists on the live site
	 *
	 * @return bool
	 */
	public function getExistsOnLive() {
		return (bool)Versioned::get_versionnumber_by_stage(get_class($this->owner), 'Live', $this->owner->ID);
	}

	/**
	 * Compares current draft with live version, and returns true if these versions differ, meaning there have been
	 * unpublished changes to the draft site.
	 *
	 * @return bool
	 */
	public function getIsModifiedOnStage() {
		// New unsaved pages could be never be published
		if($this->isNew()) return false;

		$stageVersion = Versioned::get_versionnumber_by_stage(get_class($this->owner), 'Stage', $this->owner->ID);
		$liveVersion =	Versioned::get_versionnumber_by_stage(get_class($this->owner), 'Live', $this->owner->ID);

		$isModified = ($stageVersion && $stageVersion != $liveVersion);

		return $isModified;
	}

	/**
	 * Compares current draft with live version, and returns true if no live version exists, meaning the page was never
	 * published.
	 *
	 * @return bool
	 */
	public function getIsAddedToStage() {
		// New unsaved pages could be never be published
		if($this->isNew()) return false;

		$stageVersion = Versioned::get_versionnumber_by_stage(get_class($this->owner), 'Stage', $this->owner->ID);
		$liveVersion =	Versioned::get_versionnumber_by_stage(get_class($this->owner), 'Live', $this->owner->ID);

		return ($stageVersion && !$liveVersion);
	}
    
    public function isNew() {
		/**
		 * This check was a problem for a self-hosted site, and may indicate a bug in the interpreter on their server,
		 * or a bug here. Changing the condition from empty($this->owner->ID) to !$this->owner->ID && !$this->record['ID'] fixed this.
		 */
		if(empty($this->owner->ID)) return true;

		if(is_numeric($this->owner->ID)) return false;

		return stripos($this->owner->ID, 'new') === 0;
	}
    
    /* ---- Extension hook methods ---- */
    
    public function updateSummaryFields(&$fields)
    {
        $fields['StatusField'] =  _t('TkiPublishable.Status', 'Publish Status');
    }

}
