<?php

/**
 * @deprecated
 */
class TkiSortableDo extends DataExtension {

	public function extraStatics() {
        return array(
            'db' => array(
                'SortOrder' => 'Int',
            )
        );
    }
	
	public function augmentSQL(SQLQuery &$query) {
			// Order records by SortOrder field
		foreach($query->from as $table => $dummy) {
			$query->orderby(array('sort'=>'SortOrder','dir'=>'asc'));
			break;
		}
	}
}

?>
