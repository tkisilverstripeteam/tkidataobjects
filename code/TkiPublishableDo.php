<?php

class TkiPublishableDo extends DataExtension {
	/* ---- Static variables ---- */
	
	/* ---- Instance variables ---- */
	
	/* ---- Static methods ---- */
	
	/* ---- Instance methods ---- */
	public function extraStatics() {
        return array(
            'db' => array(
                'Status' => "Enum('Draft,Published,Unpublished')"
            )
        );
    }
	/* Find records according to stage
	 * Live: Published records   
	 * Stage: Draft & Published records
	 * No modification to default (backend)
	 */
	public function augmentSQL(SQLQuery &$query) {
		$stage = Versioned::current_stage();
			
		foreach($query->from as $table => $dummy) {
			switch($stage) {
				case 'Live':
					$query->where('Status','Published');
				break;
				case 'Stage':
					$query->where("\"Status\" ='Draft' OR \"Status\" ='Published'");
					break;
				default:
					break;
			}
		}
	}

}

?>
