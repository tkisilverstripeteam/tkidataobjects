<?php

/** DataObject Versioning
 * 
 * Handles versioning for has_many DataObjects linked to data objects.
 * NB: Currently assumes available stages are the conventional "Stage" and "Live"
 * @package tkidoversioning
 */
class TkiDoVersioning extends DataExtension
{
    /* ---- Versioning ---- */

    protected function iterateVersionedObjects()
    {
       // if (!($this->owner instanceof SiteTree))
        //    return false;
        $args = func_get_args();
        $callback = array_shift($args);

        /*
         * Has many
         */
        $hasMany = $this->owner->hasMany();
        $components = $this->versionedComponents();

        foreach ($hasMany as $relation => $class) {
            if (in_array($relation, $components) && $class !== 'SiteTree' && Object::has_extension($class, 'Versioned')) {
                $callbackArgs = $args;
                array_unshift($callbackArgs, $relation, $class);
                if (method_exists($this, $callback)) {
                    call_user_func_array(array($this, $callback), $callbackArgs);
                }
            }
        }

        /*
         * Many many @todo
         */
    }

    public function versionedComponents()
    {
        return Config::inst()->get(get_class($this->owner), 'versioned_components') ?: array();
    }

    protected function findForeignKey($class)
    {
        if (empty($class))
            return null;
        $foreignRelation = null;
        $ancestry = ClassInfo::ancestry($this->owner->class);
        $ancestry = array_reverse($ancestry);

        if ($ancestry)
            // Find class with has_one relation
            foreach ($ancestry as $ancestorClass) {
                if ($ancestorClass == 'DataObject')
                    break;
                $has_one = (array) Object::uninherited_static($class, 'has_one');
                if (!empty($has_one)) {
                    $foreignRelation = array_search($ancestorClass, $has_one);
                    if (!empty($foreignRelation))
                        break;
                }
            }
        return !empty($foreignRelation) ? $foreignRelation . 'ID' : $foreignRelation;
    }

    protected function publishDataObjects($relation, $class)
    {
        // Check args
        if (empty($relation) || empty($class))
            return false;

        $foreignKey = $this->findForeignKey($class);
        if ($foreignKey) {
            $filter = "\"{$class}\".\"$foreignKey\" = {$this->owner->ID}";
            $oldRecords = Versioned::get_by_stage($class, 'Live', $filter);
            if ($oldRecords)
                foreach ($oldRecords as $record) {
                    $record->deleteFromStage('Live');
                }
            unset($record);
        }
        // Publish new records
        $newRecords = $this->owner->$relation();
        if ($newRecords)
            foreach ($newRecords as $record) {
                $record->publish('Stage', 'Live');
            }
        unset($record);
    }

    protected function unPublishDataObjects($relation, $class)
    {
        // Check args
        if (empty($relation) || empty($class))
            return false;
        // Publish new records
        $records = $this->owner->$relation();
        if ($records)
            foreach ($records as $record) {
                $record->deleteFromStage('Live');
            }
        unset($record);
    }

    protected function deleteDataObjects($relation, $class)
    {
        // Check args
        if (empty($relation) || empty($class))
            return false;
        // Publish new records
        $records = $this->owner->$relation();
        if ($records)
            foreach ($records as $record) {
                $record->delete();
            }
        unset($record);
    }

    protected function revertToLiveDataObjects($relation, $class)
    {
        // Check args
        if (empty($relation) || empty($class))
            return false;
        // Publish new records
        $records = $this->owner->$relation();
        if ($records)
            foreach ($records as $record) {
                $record->publish('Live', 'Stage', false);
                $record->writeWithoutVersion();
            }
        unset($record);
    }

    protected function duplicateDataObjects($relation, $class, $page)
    {
        // Check args
        if (empty($relation) || empty($class) || empty($page))
            return false;
        // Publish new records
        $foreignKey = $this->findForeignKey($class);
        if ($foreignKey && !empty($page->ID)) {
            $records = $this->owner->$relation();
            if ($records)
                foreach ($records as $record) {
                    $newRecord = $record->duplicate(false);
                    $newRecord->$foreignKey = $page->ID;
                    $newRecord->write();
                }
            unset($record);
        }
    }

    //Versioning DataObjectManager DataObjects
    public function onBeforePublish(&$original)
    {
        $this->iterateVersionedObjects('publishDataObjects');
    }

    public function onBeforeUnpublish()
    {
        $this->iterateVersionedObjects('unPublishDataObjects');
    }

    public function onAfterRevertToLive(&$page)
    {
        $this->iterateVersionedObjects('revertToLiveDataObjects');
    }

    public function onAfterDuplicate(&$page)
    {
        $this->iterateVersionedObjects('duplicateDataObjects', $page);
    }

    public function onBeforeDelete()
    {
        $this->iterateVersionedObjects('deleteDataObjects');
    }
}
